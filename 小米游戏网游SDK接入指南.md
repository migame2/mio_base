# <center>小米游戏网游SDK接入指南</center>

## 1.更新说明

| SDK版本 | 版本内容                                                     | 升级说明                                                     | 文档更新说明|更新时间 | 维护人 |
| ------- | ------------------------------------------------------------ | ------------------------------------------------------------ | -------- | ------ |----|
| 3.1.0 | 1.增加游戏登录前公告能力，可以灵活配置运营活动；<br>2.增加故障公告能力，以便出严重故障时发布故障处理说明；<br>3.增加小米品牌闪屏；<br>4.增加游戏角色信息提交接口；<br>5.解决在非小米手机上悬浮窗的权限问题；<br>6.修复bug；<br>7.优化登录流程；<br>8.提高支付安全性。 | 此版本更新较大，已接入老版本SDK的游戏，请仔细阅读文档，并严格按此文档步骤进行接入。 | 基于SDK3.1.0编写 | 2019.3.29 |刘洁|
| 3.1.1 | 1.优化小米账号登录，增强风控能力；<br>2.优化联网功能，提高登录成功率；<br>3.精简编译support依赖，降低接入难度；<br/>4.SDK增加接入资源自检能力。 | 此版本与3.1.0相比无接口变化，主要是libs、res及混淆文件的变化。 | 基于SDK3.1.1编写 | 2019.6.3 |刘洁|
| 3.1.2 | 1.悬浮窗增加会员与VIP标识；<br>2.悬浮窗增加“VIP上线了”消息提醒；<br>3.提高SDK接入资源自检能力。 | 此版本与3.1.1相比无接口变化，主要是界面变化;<br/>请注意替换lib、资源文件、manifest配置及更新混淆文件。 | 基于SDK3.1.2编写 | 2019.9.16 |崔俊奇|
| 3.2.0 | 1. 增加防沉迷功能;<br/>2. 优化悬浮窗;<br/>3. 减少第三方库依赖;<br/>4. 修复Bug。 | 此版本移除部分无用接口，增加了防沉迷能力，请注意阅读相关说明;<br/>请注意替换lib、资源文件、assets目录、manifest配置及更新混淆文件。 | 基于SDK3.2.0编写 | 2019.2.19 |崔俊奇|


## 2.使用说明

本指南用于指导开发者在[小米游戏中心]及[小米应用商店]中接入游戏及应用内计费的技术流程和实现,开发者也可前往[小米游戏网游SDK](https://dev.mi.com/console/doc/detail?pId=1201)获取最新SDK和文档。

本指南中： 

```
    3. 接入前准备：提供了接入应用内支付的流程及需要准备的事项。

    4. 小米游戏：提供了小米游戏及各主要组件功能介绍。

    5. 接入流程与实现：提供了游戏及应用的接入流程和技术实现，适合研发工程师阅读。

    6. 常见问题：提供了游戏及应用的接入过程中遇到的绝大部分通用问题的解决方案及最佳实践，适合研发工程师阅读。
```
### 旧版本接入升级
建议设置compileSdkVersion 为 28及以上
| 旧版 | 新版                                                     | 升级说明 |
| ------------------ | ------------------- | -
| Manifest配置 |  | <font color=#ff0000 >添加多个新Activity，修改provider配置</font> | 
| MiCommplatform.setResourceClass(String RClass); |  | 删除 | 
| MiCommplatform.getInstance().onMainActivityDestory(Context context); | MiCommplatform.getInstance().onMainActivityDestory(); | 移除入参 | 
| MiCommplatform.Init |  | 接口无变化，但<font color=#ff0000 >必须在Manifest注册的Application的onCreate中初始化，否则会造成功能异常</font> | 
| proguard.cfg |   | 新增-dontwarn com.google.** | 
| libs目录 |  | <font color=#ff0000 >升级支付宝等依赖jar，请替换并删除低版本依赖库</font> | 
| libs目录 | 解耦zxing库  | <font color=#ff0000 >请删除原来的HyZxing.jar，并将sdk依赖的所有jar复制到libs目录下</font> | 
| 无assets资源 | 新增assets资源 | <font color=#ff0000 >新增asstes资源，请注意添加</font>  | 
| res目录 | | 文件变动较多，建议整体替换 | 
|sdk退弹接口  | 改为必接功能|  <font color=#ff0000 >退弹接口为游戏社区等进行引流，必须接入，否则可能会被审 核驳回</font >|
## 3.接入前准备

开发者可根据以下项做好接入前的准备工作： 

1. 准备小米手机、平板设备。设备规格：[小米手机&平板设备规格](https://dev.mi.com/console/doc/detail?pId=938)
2. 注册开发者站帐号。流程：[开发者账号注册流程](https://dev.mi.com/console/doc/detail?pId=848)
3. 在开发者站创建应用并申请AppId、AppKey、AppSecret，AppId、AppKey用来初始化SDK、AppSecret在服务器与服务器通信签名[生成signature](https://dev.mi.com/console/doc/detail?pId=102#5.3.5)时使用，如果是游戏PackageName必须以“.mi”结尾。流程：[应用提交流程](https://dev.mi.com/console/doc/detail?pId=878)。
4. 在开发者站配置应用内支付，如果是游戏必须选择‘手机游戏’否则上线审核将会被拒绝，并配置商品代码，如果是[按金额付费](https://dev.mi.com/console/doc/detail?pId=102#5.2.2.3)必须配置回调地址，[按计费代码付费](https://dev.mi.com/console/doc/detail?pId=102#5.2.2.4)可选。

## 4.小米游戏

### 4.1小米游戏平台介绍

小米游戏平台是小米公司推出的面向所有小米手机、MIUI用户的游戏平台，整个游戏平台整合了MIUI系统、开发SDK、互联网网站、小米论坛等各方面优质资源，力求打造一个良好的安卓游戏生态系统。 小米游戏平台提供游戏下载、游戏搜索、游戏计费（支持单机游戏和网络游戏）等多种平台支撑能力，接入简单快捷，是众多游戏开发商最优的选择。

### 4.2小米账户

小米账户是小米公司为所有小米网、MIUI、米聊而开发的小米通行证账户系统，小米账户主要采取绑定手机号码或邮箱的方式，用户注册了小米账户同时会享受小米云服务、小米社区、米聊、游戏中心等多项优质免费服务。 小米账户即可使用小米ID登录，也可使用手机号码、邮箱作为用户名登录，十分方便快捷。 若没有项目账号，可以访问[https://account.xiaomi.com/](https://account.xiaomi.com/pass/serviceLogin)进行注册。

### 4.3米币系统

米币是小米公司针对虚拟商品支付而发行的一种通用代币。具有可在小米公司所有虚拟物品平台流通的特性。用户只要通过小米账号进行充值（用人民币购买米币），就可以在小米公司旗下各移动产品或合作产品平台中支付使用。1元=1米币，米币最小单位是1分，即可定价为0.01米币。 开发者将所在应用中用户消费的米币数额作为应用收入的结算依据。 目前给米币充值的方式包括：支付宝、财付通、银行卡、信用卡、话费充值卡、手机短信充值等。 用户可以在MIUI手机里内置的‘米币中心’进行充值，也可以访问 [http://mibi.xiaomi.com/](https://mibi.xiaomi.com/)进行充值。

## 5.接入流程与实现

### 5.1快速开始

在继续看文档之前，建议您先把随本文档一起分发的Demo程序（demo内的MiGameSdkSample-release.apk）安装到手机或安卓平板上，这个程序完整演示了小米应用支付的工作流程，有助于您快速理解我们SDK支付的整个流程。

请参考SampleMiGameSdk项目。

**所有的SDK的资源、依赖均在SampleMiGameSdk的sdk子module中。**

应用内支付SDK核心是mio_sdk_xxx_xxx_xxx.jar(位于libs目录下,根据不同版本,名称会有所变化,但开头会是"mio_sdk_",以实际名称为准)，需要将此jar包放入游戏App的工程中，我们已将要使用的接口封装好。

应用内支付SDK还需要一系列第三方支持的jar包，这些jar包也在libs目录下，除此之外还必需2个so文件，目前提供armeabi-v7a/armeabi/arm64-v8a/x86版本。

额外的还有MiGameCenterSDKService.apk(demo中未提供，[点此下载](http://app.mi.com/details?id=com.xiaomi.gamecenter.sdk.service))，目前在非MIUI下只提供最常用的登录(QQ和小米账号)和支付(微信和支付宝)功能。而如果用户安装了此apk并给予了相应的权限(特别是关联启动的权限)则能提供和MIUI上一样的完整的登录和支付功能，可自行决定是否引导用户安装并配置此apk(一般是在非MIUI用户反馈功能问题时引导用户进行安装和授予权限)。

![](res/SDK_cn.jpg)

### 5.2产品设计及实现说明

#### 5.2.1登录及支付

##### 5.2.1.1登录流程概述
注意：

1. 单机游戏：单机游戏开发者在支付前要调用miLogin()函数进行登陆，登陆成功后才可以调用miUniPay()方法进行支付 
2. 网络游戏：网络游戏开发者需要管理用户登录，并且需要记录用户登录状态，用户每次启动游戏时，必须调用miLogin()来判断用户会话是否超时，在游戏过程中如果需要充值可以调用miUniPay() 
3. 应用：用户每次启动应用时，必须调用miLogin()来判断用户会话是否超时，在应用运行过程中，如果需要充值可以调用miUniPay()。 登录实现请参考[5.2.2.2小米账户登录调用代码](https://dev.mi.com/console/doc/detail?pId=102#5.2.2.2)

```
调取miLogin()登录前需要做：
1. 在开发者站申请并绑定有效的appId/appKey
2. 配置应用内支付模块，否则会报告-102错误
```



**用户开户及登录流程**

![](res/login.png)

```
注意：
1.UID不是小米ID，但与小米ID有对照关系
2.请开发者一定要使用这个UID作为用户标识，不要使用本机的IMEI、IMSI或Mac地址，我们的审核团队会严格测试
```

##### 5.2.1.2 支付流程概述

计费方式：

1.按金额付费：必须配置回调地址，推荐网游使用；

2.按计费代码付费：支持可消耗类商品代码与不可消耗类商品代码，回调地址配置可选，推荐单机和应用使用。

**按金额付费流程**

![](res/pay.jpg)

#### 5.2.2 SDK调用方法

##### 5.2.2.1 初始化相关

在小米开发者创建应用并获取 AppId 、AppKey和AppSecret，创建应用时如果是游戏PackageName必须以“.mi”为后缀。 

```
注意：AppSecret是用于服务器端签名，不要在客户端里使用
```

将 SDK 包中res目录中的资源，完整添加到游戏Android工程的res目录。如果样式有冲突，可酌情修改。

将 SDK 包中libs目录下的文件放在你的项目的libs目录下(图中绿色的核心jar包会根据不同版本,名称会有所变化,以实际名称为准,部分第三方包也可能由于版本不同而有变化,以实际为准)，在 buildpath 中引用，然后对 SDK 进行初始化。

![](res/libs.png)

**注意: 需要检查下面的一致性，如果不一致会导致调用登录和其它 SDK 接口失败  **

```
1. 在小米开发者站后台配置好AppId/AppKey和包名（包名必须以.mi结尾）;
2. 检查AndroidManifest.xml里面所设置的package必须与开发者站配置的一致;
3. 必须配置应用内消费，否则无法成功登陆（会返回1515）;
4. 上传对应签名的apk到小米开发者站后台（可不申请审核）;
5. 在Application.onCreate中调用初始化方法;
6. 在游戏主activity的onCreate方法中调用onMainActivityCreate，在游戏主activity的onDestory方法中调用onMainActivityDestory。
```

**在Application.onCreate中调用初始化方法：**

```java
MiAppInfo appInfo = new MiAppInfo();
appInfo.setAppId("请申请获得");
appInfo.setAppKey("请申请获得");
MiCommplatform.Init(this, appInfo, new OnInitProcessListener() {
    @Override
            public void finishInitProcess(List<String> loginMethod, int gameConfig) {
                Log.i("Demo", "Init success");
            }

            @Override
            public void onMiSplashEnd() {
			//小米闪屏页结束回调，小米闪屏可配，无闪屏也会返回此回调，游戏的闪屏应当在收到此回调之后开始。
            }
    });
```

**在游戏主activity的onCreate方法中调用onMainActivityCreate，在游戏主activity的onDestory方法中调用onMainActivityDestory：**

```java
请确保该方法在游戏场景所在的Activity中调用，避免在类似开屏等界面中调用

@Override
	public void onCreate( Bundle savedInstanceState ) {
		super.onCreate( savedInstanceState );
		MiCommplatform.getInstance().onMainActivityCreate(this);
		setContentView( R.layout.activity_main );
    //……
  }
```

```java
@Override
	protected void onDestroy() {
		super.onDestroy();
		MiCommplatform.getInstance().onMainActivityDestory();
	}
```

**SDK所需要的权限**

```xml
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.ACCESS_NETWORK_STATE" />
<uses-permission android:name="android.permission.ACCESS_WIFI_STATE" />
<uses-permission android:name="android.permission.READ_PHONE_STATE" />
<uses-permission android:name="android.permission.INTERNET" />
<uses-permission android:name="com.xiaomi.sdk.permission.PAYMENT" />
<uses-permission android:name="com.xiaomi.permission.AUTH_SERVICE" />
<uses-permission android:name="android.permission.REQUEST_INSTALL_PACKAGES" />
```

**游戏包主题设置**
```xml
建议将AndroidManifest中application主题设置为android:theme="@android:style/Theme.Light.NoTitleBar.Fullscreen",
设置为其它主题可能导致某些弹框样式不正确，xml如下所示：
<application
    ...
	android:theme="@android:style/Theme.Light.NoTitleBar.Fullscreen">
```

**SDK所需的android support依赖**

android studio

```groovy
implementation 'com.android.support:support-v4:27.1.1'
```

eclipse

```
见02-Demo/eclipse_support/
```



**SDK所需要的AndroidManifest清单配置**

```xml
	<meta-data
            android:name="MiLinkGroupAppID"
            android:value="@integer/MiLinkGroupAppID"/>
        <activity
            android:name="com.xiaomi.gamecenter.sdk.ui.MiActivity"
            android:configChanges="orientation|screenSize"
            android:screenOrientation="behind"
            android:theme="@android:style/Theme.Translucent.NoTitleBar" >
        </activity>
        <activity
            android:name="com.xiaomi.gamecenter.sdk.ui.PayListActivity"
            android:configChanges="orientation|screenSize"
            android:exported="true"
            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
        <activity
            android:name="com.xiaomi.hy.dj.HyDjActivity"
            android:configChanges="orientation|screenSize"
            android:exported="true"
            android:theme="@android:style/Theme.Translucent.NoTitleBar" />
        <activity
            android:name="com.alipay.sdk.app.H5PayActivity"
            android:configChanges="orientation|keyboardHidden|navigation|screenSize"
            android:exported="false"
            android:screenOrientation="behind"
            android:windowSoftInputMode="adjustResize|stateHidden" />
        <!--不支持${applicationId}的请替换为包名-->
        <service
            android:name="com.xiaomi.gamecenter.push.GamePushService"
            android:exported="true">
            <intent-filter>
                <action android:name="${applicationId}.MI_GAME_PUSH"/>
            </intent-filter>
        </service>

        <receiver android:name="com.xiaomi.gamecenter.push.OnClickReceiver"
                  android:exported="true">
            <intent-filter>
                <action android:name="com.xiaomi.hy.push.client.ONCLICK"/>
            </intent-filter>
        </receiver>
        <provider
            android:name="com.xiaomi.gamecenter.sdk.utils.MiFileProvider"
            android:authorities="${applicationId}.mi_fileprovider"
            android:exported="false"
            android:grantUriPermissions="true">
            <meta-data
                android:name="android.support.FILE_PROVIDER_PATHS"
                android:resource="@xml/mio_file_paths" />
        </provider>
        <activity
            android:name="com.xiaomi.gamecenter.sdk.ui.fault.ViewFaultNoticeActivity"
            android:configChanges="orientation|screenSize"
            android:excludeFromRecents="true"
            android:screenOrientation="behind"
            android:theme="@android:style/Theme.Translucent.NoTitleBar.Fullscreen" />

        <activity
            android:name="com.xiaomi.gamecenter.sdk.ui.notice.NoticeActivity"
            android:configChanges="orientation|screenSize"
            android:excludeFromRecents="true"
            android:screenOrientation="behind"
            android:theme="@android:style/Theme.Translucent.NoTitleBar.Fullscreen" />

        <activity
            android:name="com.xiaomi.gamecenter.sdk.anti.ui.MiAntiAlertActivity"
            android:configChanges="orientation|screenSize"
            android:excludeFromRecents="true"
            android:screenOrientation="behind"
            android:theme="@android:style/Theme.Translucent.NoTitleBar">
            <intent-filter>
                <data
                    android:host="open_anti_alert"
                    android:scheme="mioauthsdk" />
                <category android:name="android.intent.category.DEFAULT" />
                <action android:name="android.intent.action.VIEW" />
            </intent-filter>
        </activity>
        <activity
            android:name="com.xiaomi.gamecenter.sdk.ui.MiPayAntiActivity"
            android:configChanges="orientation|screenSize"
            android:screenOrientation="behind"
            android:theme="@android:style/Theme.Translucent.NoTitleBar" />

        <activity android:name="com.xiaomi.gamecenter.sdk.ui.MiVerifyActivity"
            android:configChanges="orientation|screenSize"
            android:theme="@android:style/Theme.Translucent.NoTitleBar"
            android:screenOrientation="behind"/>

```

<font color=#ff0000 >Unity 4 兼容性</font> 
如果您使用 Unity 4，请在继承了UnityPlayerNativeActivity的标签中添加如下配置：
```
<activity android:name="com.unity3d.player.UnityPlayerNativeActivity"
   android:label="@string/app_name">
   ...其它配置
   <meta-data android:name="unityplayer.UnityActivity" android:value="true" />
   <meta-data android:name="unityplayer.ForwardNativeEventsToDalvik"
       android:value="true" />
</activity>
```
<font color=#ff0000 >Unity 5.6.x 兼容性</font> 
Unity5.6.x上无法显示宝箱等浮窗解决方案：
自定义UnityPlayer，重写addView方法，将SurfaceView的zOrderOnTop设为false。示例代码如下
```
/****************************自定义UnityPlayerActivity****************************/
package com.xxx.yyy;
 
import com.unity3d.player.*;
import android.os.Bundle;
 
public class CUnityPlayerActivity
    extends UnityPlayerActivity
{
    @Override
    public void onCreate(Bundle bundle) {
        requestWindowFeature(1);
        super.onCreate(bundle);
        getWindow().setFormat(2);
        //mUnityPlayer = new UnityPlayer(this);
        mUnityPlayer = new CUnityPlayer(this);
        setContentView(mUnityPlayer);
        mUnityPlayer.requestFocus();
    }

/****************************自定义UnityPlayer****************************/
package com.xxx.yyy;
 
import com.unity3d.player.*;
import android.content.ContextWrapper;
import android.view.SurfaceView;
import android.view.View;
 
public class CUnityPlayer
    extends UnityPlayer
{
    public CUnityPlayer(ContextWrapper contextwrapper) {
        super(contextwrapper);
    }
 
    public void addView(View child) {
        if (child instanceof SurfaceView) {
            ((SurfaceView)child).setZOrderOnTop(false);
        }
        super.addView(child);
    }
}

```

##### 5.2.2.2 小米账户登录调用代码

```java
MiCommplatform.getInstance().miLogin(context,
            new OnLoginProcessListener() {
                @Override
                public void finishLoginProcess(int code, MiAccountInfo arg1) {
                    switch (code) {
                    case MiErrorCode.MI_XIAOMI_PAYMENT_SUCCESS: // 登陆成功
                                                               
 												//获取用户的登陆后的UID（即用户唯一标识）
                        long uid = arg1.getUid();

                        //以下为获取session并校验流程，如果是网络游戏必须校验，如果是单机游戏或应用可选//
                        //获取用户的登陆的Session（请参考5.3.3流程校验Session有效性）
                        String session = arg1.getSessionId();

                        //请开发者完成将uid和session提交给开发者自己服务器进行session验证
                        break;

                    case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_LOGIN_FAIL:

                        // 登陆失败
                        break;

                    case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_CANCEL:

                        // 取消登录
                        break;

                    case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_ACTION_EXECUTED:

                        //登录操作正在进行中
                        break;

                    default:

                        // 登录失败
                        break;
                    }
                }
            });
```

可以通过实现OnLoginProcessListener接口来捕获登录结果。

##### 5.2.2.3 按金额付费调用

```java
	MiBuyInfo miBuyInfo = new MiBuyInfo();
        miBuyInfo.setCpOrderId(UUID.randomUUID().toString()); //订单号唯一（不为空）
        miBuyInfo.setCpUserInfo("cpUserInfo"); //此参数在用户支付成功后会透传给CP的服务器
        miBuyInfo.setAmount(10); //必须是大于1的整数，10代表10米币，即10元人民币（不为空）
                                 //用户信息，网游必须设置、单机游戏或应用可选

        Bundle mBundle = new Bundle();
        mBundle.putString(GameInfoField.GAME_USER_BALANCE, "1000"); //用户余额
        mBundle.putString(GameInfoField.GAME_USER_GAMER_VIP, "vip0"); //vip等级 
        mBundle.putString(GameInfoField.GAME_USER_LV, "20"); //角色等级
        mBundle.putString(GameInfoField.GAME_USER_PARTY_NAME, "猎人"); //工会，帮派
        mBundle.putString(GameInfoField.GAME_USER_ROLE_NAME, "meteor"); //角色名称
        mBundle.putString(GameInfoField.GAME_USER_ROLEID, "123456"); //角色id
        mBundle.putString(GameInfoField.GAME_USER_SERVER_NAME, "峡谷"); //所在服务器
        miBuyInfo.setExtraInfo(mBundle); //设置用户信息

        MiCommplatform.getInstance().miUniPay(activity, miBuyInfo,
            new OnPayProcessListener() {
                @Override
                public void finishPayProcess(int code) {
                    switch (code) {
                    case MiErrorCode.MI_XIAOMI_PAYMENT_SUCCESS: //购买成功
                        break;

                    case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_PAY_CANCEL: //取消购买
                        break;

                    case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_PAY_FAILURE: //购买失败
                        break;

                    case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_ACTION_EXECUTED: //操作正在进行中
                        break;

                    default: //购买失败

                        break;
                    }
                }
            });
```

**参数说明**

| 名称       | 用途           | 备注                                                         |
| ---------- | -------------- | ------------------------------------------------------------ |
| cpOrderId  | 开发方订单号   | 必填参数。20~100字符以内,要求必须由开发者的业务服务器生成,因订单支付成功后游戏平台服务器会直接将支付结果通知给开发者的业务服务器,通知参数的cpOrderId是重要信息。 |
| cpUserInfo | 给网游透传参数 | 必填参数。用于透传用户信息,当用户支付成功后我们会将此参数透传给开发者业务服务器(不能为null或“”) |
| amount     | 金额           | 必填参数。且数量是int型，即最少只能购买1米币对应的虚拟币。   |

##### 5.2.2.4 按计费代码购买可消耗商品（例如：血瓶，法瓶等可重复购买的商品）

```java
MiBuyInfo miBuyInfo = new MiBuyInfo();
miBuyInfo.setCpOrderId( UUID.randomUUID().toString() );//订单号唯一（不为空）
miBuyInfo.setProductCode( “productCode” );//商品代码，开发者申请获得（不为空）
miBuyInfo.setCount( 3 );//购买数量(商品数量最大9999，最小1)（不为空）

MiCommplatform.getInstance().miUniPay( activiy, miBuyInfo, 
new OnPayProcessListener()
{
        @Override
	public void finishPayProcess( int code ) {
		switch( code ) {
		case MiErrorCode.MI_XIAOMI_PAYMENT_SUCCESS://购买成功，请处理发货
		    break;
		case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_PAY_CANCEL://取消购买
		    break;
		case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_PAY_FAILURE://购买失败
		    break;
                case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_ACTION_EXECUTED://操作正在执行
    		    break;
		default://购买失败
		    break;
		}
	}
});
```

##### 5.2.2.5 按计费代码购买非可消耗商品（例如：关卡等不可重复购买的商品）

```java
MiBuyInfo miBuyInfo = new MiBuyInfo();
miBuyInfo.setCpOrderId( UUID.randomUUID().toString() );//订单号唯一（不为空）
miBuyInfo.setProductCode( “productCode” );//商品代码，开发者申请获得（不为空）
miBuyInfo.setCount( 1 );//购买数量(只能为1)（不为空）

MiCommplatform.getInstance().miUniPay(activiy, miBuyInfo, 
new OnPayProcessListener()
{
        @Override
	public void finishPayProcess( int code ) {
		switch( code ) {
		case MiErrorCode.MI_XIAOMI_PAYMENT_SUCCESS://购买成功，请处理发货
		     break;
		case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_PAY_CANCEL://取消购买
     		    break;
		case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_PAY_FAILURE://购买失败
     		    break;
		case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_PAY_REPEAT://已购买过，无需购买，可直接使用
     		    break;
                case MiErrorCode.MI_XIAOMI_PAYMENT_ERROR_ACTION_EXECUTED://操作正在执行
     		    break;
		default://购买失败
     		    break;
		}
	}
});
```

**参数说明**

| 名称        | 用途         | 备注                                                         |
| ----------- | ------------ | ------------------------------------------------------------ |
| cpOrderId   | 开发方订单号 | 20~100字符以内,开发方生成,要求不能重复,可根据开发方规则自行生成。 |
| productCode | 商品编号     | 规则要求:可由数字 0-9,字母 a-zA-Z 以及特殊字符”_”,”.”,”-”组成, 长度8~40位,同一款 游戏内 productCode要求唯一,区分字母大小写。建议使用com.xiaomi.migc.xxx 的格式命名。调用时请不要弄混非消耗类商品和可消耗类商品的productCode。 |
| count       | 购买数量     | 非消耗类商品,取值=1 可消耗类商品,取值>=1                     |

##### 5.2.2.6 退出游戏

 <font color=#ff0000 >该功能建议接入，否则可能在审核时被驳回</font>
 sdk提供了退出游戏弹框，为游戏玩家社区进行引流，接口如下：

```java
MiCommplatform.getInstance().miAppExit( activiy, new OnExitListner()
    {
        @Override
        public void onExit( int code )
        {
            if ( code == MiErrorCode.MI_XIAOMI_EXIT )
            {
                android.os.Process.killProcess( android.os.Process.myPid() );
            }
        }
} );
```

##### 5.2.2.6 提交角色信息

调用时机：

1. 创建⻆色/已有⻆色进入游戏后提交游戏⻆色数据 ；
2. 游戏内升级时调用提交游戏⻆色数据。



```java
RoleData data = new RoleData();
data.setLevel("1");
data.setRoleId("1000");
data.setRoleName("方天");
data.setServerId("0001");
data.setServerName("国服1");
data.setZoneId("z1");
data.setZoneName("蓬莱仙岛");
MiCommplatform.getInstance().submitRoleData(MainActivity.this, data);
```

#### 5.2.3 混淆文件说明

需要注意，SDK 包是以 jar 包提供给开发者，此jar包本身已为混淆状态，您在混淆自己游戏的 APK包时，需要在proguard.cfg 里加入，以避免二次混淆。

```
-keepattributes InnerClasses,Signature,Exceptions,Deprecated,*Annotation*


-dontwarn android.**
-dontwarn com.google.**
-keep class android.** {*;}
-keep class com.google.** {*;}
-keep class com.android.** {*;}
-dontwarn org.apache.**
-keep class org.apache.** { *; }
-keep class sun.misc.Unsafe { *; }
-keep class com.google.** {*;}

-keep public class android.arch.core.internal.FastSafeIterableMap
-keep public class android.arch.core.util.Function
-keep public class android.arch.lifecycle.Lifecycle
-keep public class android.arch.lifecycle.Observer
-keep public class android.arch.lifecycle.ReportFragment
-keep public class android.arch.lifecycle.ViewModel
-keep public class android.support.v4.app.Fragment
-keep public class android.support.annotation.AnimatorRes
-keep public class android.support.v4.app.ActivityCompat
-keep public class android.support.design.widget.CoordinatorLayout
-keep public class android.support.v4.app.AppLaunchChecker
-keep public class android.support.v4.app.BackStackState


#-libraryjars libs/alipaySdk.jar
-dontwarn com.alipay.**
-keep class com.alipay.** {*;}
-keep class com.ut.device.** {*;}
-keep class com.ta.utdid2.** {*;}

#-libraryjars libs/eventbus-3.jar
-keep class org.greenrobot.eventbus.** { *; }
-keep class de.greenrobot.event.** { *; }
-keep class de.greenrobot.dao.** {*;}

-keepclassmembers class ** {
    public void onEvent*(**);
    void onEvent*(**);
}

-keepclassmembers class ** {
    @org.greenrobot.eventbus.Subscribe <methods>;
}
-keep enum org.greenrobot.eventbus.ThreadMode { *; }
# Only required if you use AsyncExecutor
-keepclassmembers class * extends org.greenrobot.eventbus.util.ThrowableFailureEvent {
    <init>(java.lang.Throwable);
}
#-libraryjars libs/wechat.jar
-keep class com.tencent.** {*;}

#-libraryjars libs/glide.jar
-keep class com.bumptech.glide.** {*;}

-dontwarn com.xiaomi.**
-keep class com.xiaomi.** {*;}
-keep class com.mi.** {*;}
-keep class com.wali.** {*;}
-keep class cn.com.wali.** {*;}
-keep class miui.net.**{*;}
-keep class org.xiaomi.** {*;}



#保留位于View类中的get和set方法
-keepclassmembers public class * extends android.view.View{
    void set*(***);
    *** get*();
}
#保留在Activity中以View为参数的方法不变
-keepclassmembers class * extends android.app.Activity{
    public void *(android.view.View);
}
#保留实现了Parcelable的类名不变，
-keep class * implements android.os.Parcelable{
    public static final android.os.Parcelable$Creator *;
}
#保留R$*类中静态成员的变量名
-keep class **.R$* {*;}

-dontwarn android.support.**
-keep class **.R$styleable{*;}

```



### 5.3 服务器接口

1. 按金额付费，以小米应用支付服务器通知的结果为准为用户进行虚拟币充值。所以必须提供接收订单支付结果通知的地址（必选）
2. 按计费代码计费，也可以提供接收订单支付结果通知的地址（可选）
3. 服务器代码示例[下载地址](http://f5.market.mi-img.com/download/MiPass/009284c2a7bae5d939988575b0d56f72d0d4374f4/ServerDemo.zip)。 包含签名实现，登录验证，订单查询，回调接口，有php和java的示例

#### 5.3.1 订单支付结果通知

##### 5.3.1.1 流程说明

此接口由开发者负责开发并在开发者后台进行配置。在订单支付成功后，小米应用支付服务器会将支付结果通知给开发者预先提供的服务器上。若开发者所提供的服务器地址不可用，在一定时间段内游戏平台服务器会按照周期进行轮询（前10次，每分钟通知1次；10次后每小时通知1次）。 具体流程如下：

![](res/server_pay.jpg)

注：由于是异步通知模型，（3）和（4）不一定是按序号产生。因此（4）和（5）需要进行轮询处理或者使用接口进行支付结果查询。 相比后面提到的开发者主动查询订单的模式，我们推荐使用此模式。

##### 5.3.1.2 接口及参数说明

1. 接口地址：各开发者服务器的通知地址（提前申请，在小米开发者站进行配置，要求配置https接口）
2. 请求方法：GET

**请求参数说明**

| 名称               | 重要性 | 说明                                                         |
| ------------------ | ------ | ------------------------------------------------------------ |
| appId              | 必须   | 游戏ID                                                       |
| cpOrderId          | 必须   | 开发商订单ID                                                 |
| cpUserInfo         | 可选   | 开发商透传信息                                               |
| uid                | 必须   | 用户ID                                                       |
| orderId            | 必须   | 游戏平台订单ID                                               |
| orderStatus        | 必须   | 订单状态，TRADE_SUCCESS 代表成功                             |
| payFee             | 必须   | 支付金额，单位为分，即0.01米币                               |
| productCode        | 必须   | 商品代码                                                     |
| productName        | 必须   | 商品名称                                                     |
| productCount       | 必须   | 商品数量                                                     |
|                    | 必须   | 支付时间,格式 yyyy-MM-dd HH:mm:ss                            |
| orderConsumeType   | 可选   | 订单类型：10：普通订单11：直充直消订单                       |
| partnerGiftConsume | 可选   | 使用游戏券金额 （如果订单使用游戏券则有,long型），如果有则参与签名 |
| signature          | 必须   | 签名,签名方法见后面说明                                      |

**注意：如果开发者允许使用游戏礼券则必须使用partnerGiftConsume参数，否则使用游戏礼券的消费订单会出现掉单情况。**

例如：

```
https://ccc.com/notify.do?appId=2882303761517239138&cpOrderId=9786bffc-996d-4553-aa33-f7e92c0b29d5&orderConsumeType=10&orderId=21140990160359583390&orderStatus=TRADE_SUCCESS&payFee=1&payTime=2014-09-05%2015:20:27&productCode=com.demo_1&productCount=1&productName=%E9%93%B6%E5%AD%901%E4%B8%A4&uid=100010&signature=1388720d978021c20aa885d9b3e1b70cec751496
```

**返回参数说明**

| 名称    | 重要性 | 说明                                                         |
| ------- | ------ | ------------------------------------------------------------ |
| errcode | 必须   | 状态码:<br/>200   成功
1506 cpOrderId 错误
1515 appId 错误
1516 uid 错误
1525 signature 错误
3515 订单信息不一致，用于和 CP 的订单校验 |
| errMsg  | 可选   | 错误信息                                                     |

注意：对于同一个订单号的多次通知，开发商要自己保证只处理一次发货。 例如： {"errcode":200}

##### 服务器IP地址

```
42.62.48.246

223.202.68.237

42.62.103.0/26 (42.62.103.1 ~ 42.62.103.62)

120.134.34.0/26 (120.134.34.1 ~ 120.134.34.62)

124.251.57.0/24 (124.251.57.0 ~ 124.251.57.255)
```

请开发商服务器开发人员加到ip白名单内，以免因为ip限制造成回调不成功。

#### 5.3.2 主动查询订单支付状态接口

此接口由小米为开发者提供。

##### 5.3.2.1 流程说明

![](res/server_query.jpg)

##### 5.3.2.2 接口及参数说明

1. 接口地址：https://mis.migc.xiaomi.com/api/biz/service/queryOrder.do
2. 请求方法:GET

**请求参数说明**

| 名称      | 重要性 | 说明                    |
| --------- | ------ | ----------------------- |
| appId     | 必须   | 游戏ID                  |
| cpOrderId | 必须   | 开发商订单ID            |
| uid       | 必须   | 用户ID                  |
| signature | 必须   | 签名,签名方法见后面说明 |

例如：

```
https://mis.migc.xiaomi.com/api/biz/service/queryOrder.do?appId=2882303761517239138&cpOrderId=9786bffc-996d-4553-aa33-f7e92c0b29d5&uid=100010&signature=e91d68b864b272b6ec03f35ee5a640423d01a0a1
```

**正确返回参数说明**

| 名称             | 重要性 | 说明                                                         |
| ---------------- | ------ | ------------------------------------------------------------ |
| appId            | 必须   | 游戏ID                                                       |
| cpOrderId        | 必须   | 开发商订单ID                                                 |
| cpUserInfo       | 可选   | 开发商透传信息                                               |
| uid              | 必须   | 用户ID                                                       |
| orderId          | 必须   | 游戏平台订单ID                                               |
| orderStatus      | 必须   | 订单状态，<br>TRADE_SUCCESS 代表成功<br>WAIT_BUYER_PAY 代表未支付<br>REPEAT_PURCHASE 订购关系已经存在 |
| payFee           | 必须   | 支付金额，单位为分，即0.01米币                               |
| productCode      | 必须   | 商品代码                                                     |
| productName      | 必须   | 商品名称                                                     |
| productCount     | 必须   | 商品数量                                                     |
| payTime          | 必须   | 支付时间，格式 yyyy-MM-dd HH:mm:ss                           |
| orderConsumeType | 可选   | 订单类型：10：普通订单11：直充直消订单                       |
| signature        | 必须   | 签名,签名方法见后面说明                                      |

例如：

```json
{
"signature": "eb30240cff8c66f856ec0e48354aa670b8cf037f",
"uid": "100010",
"appId": 2882303761517239300,
"cpOrderId": "9786bffc-996d-4553-aa33-f7e92c0b29d5",
"productCode": "com.demo_1",
"orderStatus": "TRADE_SUCCESS",
"productName": "%E9%93%B6%E5%AD%901%E4%B8%A4",
"productCount": 1,
"orderConsumeType": "10",
"orderId": "21140990160359583390",
"payFee": 1,
"payTime": "2014-09-05 15:20:27"
}
```

**错误返回参数说明**

| 名称    | 重要性 | 说明                                                         |
| ------- | ------ | ------------------------------------------------------------ |
| errcode | 必须   | 状态码:<br/>1506 cpOrderId 错误
1515 appId 错误
1516 uid 错误
1525 signature 错误 |
| errMsg  | 可选   | 错误信息                                                     |

#### 5.3.3 用户session验证接口

此接口由小米应用支付为开发者提供，用于验证登录账户的有效性。

注意：用户的唯一标识是通过SDK获得的uid，而不是Session，Session用于校验登录验证的有效性，必须经过SDK、游戏中心服务器、开发者服务器进行三方验证，如果Session失效，需要重新调用miLogin()进行登录。

##### 5.3.3.1 流程说明

![](res/server_session.jpg)

##### 5.3.3.2 接口及参数说明

1. 接口地址：https://mis.migc.xiaomi.com/api/biz/service/loginvalidate
2. 请求方法:POST
3. Headers:Content-Type: application/x-www-form-urlencoded

**请求参数说明**

| 名称      | 重要性 | 说明                    |
| --------- | ------ | ----------------------- |
| appId     | 必须   | 游戏ID                  |
| session   | 必须   | 用户sessionID           |
| uid       | 必须   | 用户ID                  |
| signature | 必须   | 签名,签名方法见后面说明 |

 例如：

```
POST https://mis.migc.xiaomi.com/api/biz/service/loginvalidate
     appId=2882303761517239138&session=1nlfxuAGmZk9IR2L&uid=100010&signature=b560b14efb18ee2eb8f85e51c5f7c11f697abcfc
```

**返回参数说明**

| 名称    | 重要性 | 说明                                                         |
| ------- | ------ | ------------------------------------------------------------ |
| errcode | 必须   | 状态码:<br>200   验证正确 <br>1515 appId 错误(注意格式问题:比如appId必须是数字以及是否没传)<br>1516 uid 错误<br>1520 session 错误<br>1525 signature 错误<br>4002 appid, uid, session 不匹配(常见为session过期) |
| errMsg  | 可选   | 错误信息                                                     |
| adult   | 可选   | 用户实名标识:<br>406 非身份证实名方式<br>407 实名认证通过，年龄大于18岁<br>408 实名认证通过，年龄小于18岁<br>409 未进行实名认证
|age|可选|用户年龄
例如：

```json
{ "errcode": 200,"adult":409 }
```

#### 5.3.4 接口格式说明

输入参数：?参数1=值1&参数2=值2&....&参数n=值n，如果遇到文本参数值,需要根据情况对参数值做 UrlEncode。

返回参数：在回调的时候，是 http get方式发送请求，参数拼接在url后面，你们的服务器返回的数据 要求是json格式的,如：{"errcode":200}这种格式。 

#### 5.3.5 signature签名方法说明

1. 生成带签名字符串表中各参数按字母顺序排序（不包含signature），如果第一个字母相同，按第二个字母排序，依次类推。排序后拼接成par1=val1&par2=val2&par3=val3的格式,所生成的字符串即为待签名的字符串。没有值的参数请不要参与签名。由于有些数据根据HTTP协议需求,需要进行URLencoding,这样接收方才可以接收到正确的参数,但**如果这个参数参与签名,那么待签名字符串必须是字符串原值而非URLencoding的值**。 例如(**具体参数请以收到的为准**，**不要依据此示例修改参数**)：

**在订单通知接口收到的回调信息如下：**

```
appId=2882303761517239138&cpOrderId=9786bffc-996d-4553-aa33-f7e92c0b29d5&orderConsumeType=10&orderId=21140990160359583390&orderStatus=TRADE_SUCCESS&payFee=1&payTime=2014-09-05%2015:20:27&productCode=com.demo_1&productCount=1&productName=%E9%93%B6%E5%AD%901%E4%B8%A4&uid=100010&signature=1388720d978021c20aa885d9b3e1b70cec751496

```

**这时候需要对每个参数的值进行URLdecode，decode后的字符如下：**

```
appId=2882303761517239138&cpOrderId=9786bffc-996d-4553-aa33-f7e92c0b29d5&orderConsumeType=10&orderId=21140990160359583390&orderStatus=TRADE_SUCCESS&payFee=1&payTime=2014-09-05 15:20:27&productCode=com.demo_1&productCount=1&productName=银子1两&uid=100010&signature=1388720d978021c20aa885d9b3e1b70cec751496

```

**而需要进行签名的字符串为：**

```
appId=2882303761517239138&cpOrderId=9786bffc-996d-4553-aa33-f7e92c0b29d5&orderConsumeType=10&orderId=21140990160359583390&orderStatus=TRADE_SUCCESS&payFee=1&payTime=2014-09-05 15:20:27&productCode=com.demo_1&productCount=1&productName=银子1两&uid=100010

```

2. 签名算法以AppSecret作为key，使用hmac-sha1带密钥(secret)的哈希算法对代签字符串进行签名计算。签名的结果由16进制表示。

   hmac-sha1 带密钥(secret)哈希算法的java实现参考：

   ```java
   import javax.crypto.Mac;
   import javax.crypto.SecretKey;
   import javax.crypto.spec.SecretKeySpec;
   
   public class HmacSHA1Encryption {
   	private static final String MAC_NAME = "HmacSHA1";
   	private static final String ENCODING = "UTF-8";
   	/**
   	 * 使用 HMAC-SHA1 签名方法对对encryptText进行签名
   	 * @param encryptText 被签名的字符串
   	 * @param encryptKey 密钥
   	 * @return 返回被加密后的字符串
   	 * @throws Exception
   	 */
   	public static String HmacSHA1Encrypt( String encryptText, 
   String encryptKey ) throws Exception{
   		byte[] data = encryptKey.getBytes( ENCODING );
   		// 根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
   		SecretKey secretKey = new SecretKeySpec( data, MAC_NAME );
   		// 生成一个指定 Mac 算法 的 Mac 对象
   		Mac mac = Mac.getInstance( MAC_NAME );
   		// 用给定密钥初始化 Mac 对象
   		mac.init( secretKey );
   		byte[] text = encryptText.getBytes( ENCODING );
   		// 完成 Mac 操作
   		byte[] digest = mac.doFinal( text );
   		StringBuilder sBuilder = bytesToHexString( digest );
   		return sBuilder.toString();
   	}
   
   
   	/**
   	 * 转换成Hex
   	 * 
   	 * @param bytesArray
   	 */
   	public static StringBuilder bytesToHexString( byte[] bytesArray ){
   		if ( bytesArray == null ){
   			return null;
   		}
   		StringBuilder sBuilder = new StringBuilder();
   		for ( byte b : bytesArray ){
   			String hv = String.format("%02x", b);
   			sBuilder.append( hv );
   		}
   		return sBuilder;
   	}
   
   	/**
   	 * 使用 HMAC-SHA1 签名方法对对encryptText进行签名
   	 * 
   	 * @param encryptData 被签名的字符串
   	 * @param encryptKey 密钥
   	 * @return 返回被加密后的字符串
   	 * @throws Exception
   	 */
   	public static String hmacSHA1Encrypt( byte[] encryptData, String encryptKey ) throws Exception{
   		byte[] data = encryptKey.getBytes( ENCODING );
   		// 根据给定的字节数组构造一个密钥,第二参数指定一个密钥算法的名称
   		SecretKey secretKey = new SecretKeySpec( data, MAC_NAME );
   		// 生成一个指定 Mac 算法 的 Mac 对象
   		Mac mac = Mac.getInstance( MAC_NAME );
   		// 用给定密钥初始化 Mac 对象
   		mac.init( secretKey );
   		// 完成 Mac 操作
   		byte[] digest = mac.doFinal( encryptData );
   		StringBuilder sBuilder = bytesToHexString( digest );
   		return sBuilder.toString();
   	}
   }
   ```

   